package org.mcvly.bitdemo.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BitdemoRestApplication {
    public static void main(String[] args) {
        SpringApplication.run(BitdemoRestApplication.class, args);
    }
}
