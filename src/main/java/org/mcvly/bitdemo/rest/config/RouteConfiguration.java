package org.mcvly.bitdemo.rest.config;

import org.mcvly.bitdemo.rest.web.BlocksHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.server.RequestPredicates.GET;

@Configuration
public class RouteConfiguration {

    private final BlocksHandler blocksHandler;

    @Autowired
    public RouteConfiguration(BlocksHandler blocksHandler) {
        this.blocksHandler = blocksHandler;
    }

    @Bean
    public RouterFunction<ServerResponse> fluxRouterFunction() {
        return RouterFunctions
                .route(GET("/blocks"), blocksHandler::fetchBlocks);
    }

}
