package org.mcvly.bitdemo.rest.service;

import org.mcvly.bitdemo.rest.repository.BlockRepository;
import org.mcvly.bitdemo.rest.web.Block;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

import java.util.Optional;
import java.util.function.Function;

@Service
public class DefaultBlockService implements BlockService {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultBlockService.class);

    private final BlockRepository blockRepository;
    private final Long averageBlockTime;

    @Autowired
    public DefaultBlockService(BlockRepository blockRepository, @Value("${block.time.average}") Long averageBlockTime) {
        this.blockRepository = blockRepository;
        this.averageBlockTime = averageBlockTime;
    }

    @Override
    public Flux<Block> getBlocks(int blockCount) {
        return blockRepository.readLastNBlocks(blockCount + 1) // need one extra element to count difference
                .map(new BlocksMapper())
                .filter(Optional::isPresent)
                .map(Optional::get);
    }

    private Long avgTime(Long v) {
        return (long) (100.0 * v / averageBlockTime);
    }

    private class BlocksMapper implements Function<String, Optional<Block>> {
        Optional<Long> previousTime = Optional.empty();

        @Override
        public Optional<Block> apply(String s) {
            try {
                String[] split = s.split(";");
                Long currentTime = Long.parseLong(split[0]);
                Long currentId = Long.parseLong(split[1]);
                Optional<Long> value = previousTime.map(previous -> currentTime - previous);
                previousTime = Optional.of(currentTime);
                return value.map(v -> new Block(currentId, avgTime(v)));
            } catch (Exception e) {
                LOG.warn("Exception while parsing block '{}'", s, e);
                return Optional.empty();
            }
        }
    }
}
