package org.mcvly.bitdemo.rest.service;

import org.mcvly.bitdemo.rest.web.Block;
import reactor.core.publisher.Flux;

public interface BlockService {

    Flux<Block> getBlocks(int blockCount);

}
