package org.mcvly.bitdemo.rest.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.ReactiveListOperations;
import org.springframework.data.redis.core.ReactiveRedisOperations;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public class ReactiveRedisBlockRepository implements BlockRepository {

    private final ReactiveRedisOperations<String, String> operations;
    private final String key;

    @Autowired
    public ReactiveRedisBlockRepository(
            ReactiveRedisOperations<String, String> operations,
            @Value("${block.redis.key}") String key) {
        this.operations = operations;
        this.key = key;
    }

    @Override
    public Flux<String> readLastNBlocks(int count) {
        ReactiveListOperations<String, String> listOps = operations.opsForList();
        return listOps.range(key, -1 * count,-1);
    }

}
