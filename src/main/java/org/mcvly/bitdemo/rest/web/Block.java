package org.mcvly.bitdemo.rest.web;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class Block {

    @JsonProperty("blockId")
    private final Long blockId;

    @JsonProperty("blockTime")
    private final Long blockTime;

    @JsonCreator
    public Block(@JsonProperty("blockId") Long blockId, @JsonProperty("blockTime") Long blockTime) {
        this.blockId = blockId;
        this.blockTime = blockTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Block block = (Block) o;
        return Objects.equals(blockId, block.blockId) &&
                Objects.equals(blockTime, block.blockTime);
    }

    @Override
    public int hashCode() {

        return Objects.hash(blockId, blockTime);
    }
}
