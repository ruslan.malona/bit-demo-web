package org.mcvly.bitdemo.rest.web;

import org.mcvly.bitdemo.rest.service.BlockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static org.springframework.http.MediaType.APPLICATION_JSON;

@Component
public class BlocksHandler {

    private final BlockService blockService;
    private final Integer defaultSize;

    @Autowired
    public BlocksHandler(BlockService blockService, @Value("${block.list.size}") Integer defaultSize) {
        this.blockService = blockService;
        this.defaultSize = defaultSize;
    }

    public Mono<ServerResponse> fetchBlocks(ServerRequest request) {
        Integer blockCount = getBlockCount(request);
        Flux<Block> blockStream = blockService.getBlocks(blockCount);
        return ServerResponse.ok()
                .contentType(APPLICATION_JSON)
                .body(blockStream, Block.class);
    }

    private Integer getBlockCount(ServerRequest request) {
        return request.queryParam("size").map(Integer::parseInt).orElse(defaultSize);
    }

}
