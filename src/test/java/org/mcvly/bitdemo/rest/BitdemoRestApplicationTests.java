package org.mcvly.bitdemo.rest;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.lettuce.core.RedisClient;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.sync.RedisCommands;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mcvly.bitdemo.rest.web.Block;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.json.ObjectContent;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

@ActiveProfiles(profiles = { "dev" })
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class BitdemoRestApplicationTests {

    private RedisClient redisClient = RedisClient.create("redis://localhost");

    @Autowired
    private TestRestTemplate restTemplate;

    @Value("${block.redis.key}") String key;

    private JacksonTester<List<Block>> json;

    @Before
    public void setup() {
        StatefulRedisConnection<String, String> connect = redisClient.connect();
        RedisCommands<String, String> commands = connect.sync();
        commands.del(key);
        commands.rpush(key, "0;0", "600;1", "900;2", "1350;3", "1950;4", "2850;5", "3450;6");
        connect.close();
        redisClient.shutdown();
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        JacksonTester.initFields(this, objectMapper);
    }

    @Test
	public void testGetFive() throws IOException {
        String res = this.restTemplate.getForObject("/blocks?size=5", String.class);
        ObjectContent<List<Block>> parse = json.parse(res);
        List<Block> blocks = parse.getObject();
        assertThat(blocks.size(), is(5));
        List<Block> expected = Arrays.asList(new Block(2L, 50L), new Block(3L, 75L),
                new Block(4L, 100L), new Block(5L, 150L), new Block(6L, 100L));
        assertThat(blocks, is(expected));
    }

    @Test
    public void testGetAll() throws IOException {
        String res = this.restTemplate.getForObject("/blocks", String.class);
        ObjectContent<List<Block>> parse = json.parse(res);
        List<Block> blocks = parse.getObject();
        assertThat(blocks.size(), is(6));
        assertEquals(new Block(6L, 100L), blocks.get(5));
    }

}
