package org.mcvly.bitdemo.rest.service;

import org.junit.Test;
import org.mcvly.bitdemo.rest.repository.BlockRepository;
import org.mcvly.bitdemo.rest.web.Block;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.util.Arrays;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DefaultBlockServiceTest {

    Long averageBlockTime = 3L;
    BlockRepository blockRepository = mock(BlockRepository.class);
    BlockService service = new DefaultBlockService(blockRepository, averageBlockTime);

    @Test
    public void getAllBlockTimestamps() {
        Flux<String> flux = Flux.fromIterable(Arrays.asList("100;1", "101;2", "103;3", "106;4", "110;5"));
        when(blockRepository.readLastNBlocks(5)).thenReturn(flux);

        StepVerifier.create(service.getBlocks(4))
                .expectNext(new Block(2L, 33L), new Block(3L, 66L),
                        new Block(4L, 100L), new Block(5L, 133L))
                .verifyComplete();
    }

    @Test
    public void shouldSkipCorruptedBlock() {
        Flux<String> flux = Flux.fromIterable(Arrays.asList("100;1", "101;2", "1033", "106;4", "110;5"));
        when(blockRepository.readLastNBlocks(5)).thenReturn(flux);

        StepVerifier.create(service.getBlocks(4))
                .expectNext(new Block(2L, 33L),
                        new Block(4L, 166L), new Block(5L, 133L))
                .verifyComplete();
    }


}