package org.mcvly.bitdemo.rest.repository;

import reactor.core.publisher.Flux;

public interface BlockRepository {

    Flux<String> readLastNBlocks(int count);

}
